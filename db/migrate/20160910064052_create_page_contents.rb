class CreatePageContents < ActiveRecord::Migration[5.0]
  def change
    create_table :page_contents do |t|
      t.references :page
      t.string :content_type
      t.text :content

      t.timestamps
    end
  end
end
