require 'open-uri'

module Lim::PageParser
  extend self

  def parse(url)
    html = open(url)
    doc = Nokogiri::HTML(html)

    h1 = doc.css('h1').map { |h| { type: :h1, content: h.text } }
    h2 = doc.css('h2').map { |h| { type: :h2, content: h.text } }
    h3 = doc.css('h3').map { |h| { type: :h3, content: h.text } }
    links = doc.css('a').map { |l| { type: :link, content: l.attributes['href'].value } }

    h1 + h2 + h3 + links
  end
end
