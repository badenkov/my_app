class PageIndexingJob < ApplicationJob
  queue_as :default

  def perform(page_id)
    page = Page.find(page_id)

    if page
      Page.transaction do
        page.index!
        begin
          Lim::PageParser.parse(page.url).each do |c|
            page.contents.create!(content_type: c[:type], content: c[:content])
          end
        rescue => e
          # Here we send exception to exception tracker - Rollbar for example
          # Rollbar.error(e)
          raise ActiveRecord::Rollback
        end
        page.ready!
      end
    end
  end
end
