class PageSerializer < ActiveModel::Serializer
  attributes :id, :url, :state

  has_many :contents
end
