class Page::ContentSerializer < ActiveModel::Serializer
  attributes :content_type, :content

  belongs_to :page
end
