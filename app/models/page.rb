class Page < ApplicationRecord
  include AASM

  has_many :contents

  aasm column: :state do
    state :new, initial: true
    state :indexing
    state :indexed

    event :index do
      transitions from: :new, to: :indexing
    end

    event :ready do
      transitions from: :indexing,  to: :indexed
    end
  end

  validates :url, presence: true
end
