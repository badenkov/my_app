class Page::Content < ApplicationRecord
  extend Enumerize

  belongs_to :page

  enumerize :content_type, in: [:h1, :h2, :h3, :link]

  validates :content_type, presence: true
  validates :content, presence: true
end
