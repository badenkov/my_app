# README

Welcome to test project!

## REST services

### Add page for indexing

  POST /v1/pages.json

  {:page { :url <page_url> } }

### Get pages

  GET /v1/pages.json

## Requirements

* ruby >= 2.2.2

## Up

    git clone git@bitbucket.org:badenkov/my_app.git my_app
    cd my_app
    bundle install
    bin/rake db:migrate
    bin/rails server

for testing run:

    bin/rails test
