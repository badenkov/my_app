require 'test_helper'

class PageIndexingJobTest < ActiveJob::TestCase
  test 'page indexing job' do
    page = Page.create(url: Rails.root.join('test', 'fixtures', 'files') + 'page.html')

    PageIndexingJob.perform_now(page.id)

    page.contents.reload
    result = page.contents.map { |c| {type: c.content_type, content: c.content } }
    assert result.count == 5
    assert_includes result, {type: :h1, content: "Header 1"}
    assert_includes result, {type: :h2, content: "Header 2"}
    assert_includes result, {type: :h3, content: "Header 3"}
    assert_includes result, {type: :link, content: "https://google.com"}
    assert_includes result, {type: :link, content: "https://amazon.com"}
  end
end
