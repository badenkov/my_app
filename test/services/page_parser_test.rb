require 'test_helper'

class PageParserTest < ActiveSupport::TestCase

  test "parsing html document" do
    result = ::Lim::PageParser.parse(Rails.root.join('test', 'fixtures', 'files') + 'page.html')

    assert_includes result, {type: :h1, content: "Header 1"}
    assert_includes result, {type: :h2, content: "Header 2"}
    assert_includes result, {type: :h3, content: "Header 3"}
    assert_includes result, {type: :link, content: "https://google.com"}
    assert_includes result, {type: :link, content: "https://amazon.com"}
  end
end
