FactoryGirl.define do
  factory :"page/content", class: 'Page::Content' do
    association :page
    content_type "h1"
    content
  end
end
