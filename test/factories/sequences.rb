FactoryGirl.define do
  sequence :url do |n|
    "url#{n}.example.com"
  end

  sequence :content do |n|
    "content text #{n}"
  end
end
