require 'test_helper'
require 'net/http'

class V1::PagesControllerTest < ActionDispatch::IntegrationTest
  include ActiveJob::TestHelper

  test "GET /v1/pages" do
    pages = create_list :page, 2
    pages.each do |page|
      create :"page/content", page: page
    end

    get "/v1/pages"

    assert_equal 200, status
    body = JSON.parse(response.body)
    assert body["pages"].count == 2
    body["pages"].each do |page|
      assert_not_empty page["contents"]
    end
  end

  test "POST /v1/pages" do
    clear_enqueued_jobs
    page = build(:page)

    assert_enqueued_with(job: PageIndexingJob) do
      post "/v1/pages",
        params: { page: page },
        as: :json
    end

    assert_equal 201, status
    body = JSON.parse(response.body)
    assert body["page"]["url"] = page[:url]
  end


  test "POST /v1/pages - when attributes not valid" do
    post "/v1/pages",
      params: { page: { title: "Test"}},
      as: :json

    assert_equal 422, status
    body = JSON.parse(response.body)
    assert_not_empty body["errors"]
  end
end
